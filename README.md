# Gatling Template JDBC 
## Содержание
* [Установка тестируемого приложения](#установка-тестируемого-приложения)
* [Подключение psql](#подключение-psql)
* [Блокировки в БД](#блокировки-в-бд)
  * [Блокировки строк](#блокировки-строк)
  * [Список ожиданий процессов](#список-ожиданий-процессов)
* [Просмотр плана запросов](#просмотр-плана-запросов)
* [Дополнительная информация](#дополнительная-информация)
  * [Блокировки](#блокировки)
  * [Мониторинг](#мониторинг)
* [Запуск сценариев нагрузки](#запуск-сценариев-нагрузки)

## Установка тестируемого приложения
Выполнить 1 часть: https://gitlab.com/tinkoffperfworkshop/part-1/cheat-sheet

Для начала работы вам надо поднять тестовою БД. Мы будем использовать для тестирования PostgreSQL DB. 
 
* Клонировать репозиторий
```shell script
git clone https://gitlab.com/tinkoffperfworkshop/part-2/gatling-template-jdbc.git
```

* Перейти в директорию с проектом
```shell script
cd gatling-template-jdbc/docker
```

* Поднять контейнеры с окружением
```shell script
docker-compose up -d
```

<details>
<summary>Пример успешного запуска</summary>

```shell script
$ docker-compose up -d
Creating postgresql ... done
```
</details>

* Проверить что все контейнеры поднялись:
```shell script
docker-compose ps
```

<details>
<summary>Пример успешного запуска</summary>

```shell script
$ docker-compose ps -a
   Name                 Command              State           Ports
---------------------------------------------------------------------------
postgresql   docker-entrypoint.sh postgres   Up      0.0.0.0:5432->5432/tcp
```

</details>

В результате у вас будет поднята БД PostgreSQL на 5432 порту доступный по адресу `jdbc:postgresql://localhost:5432/postgres`.
Настройки авторизации заданы в файле `docker-compose.yml`. По умолчанию будет создана БД `loadtest`, пользователь `docker` с паролем `mysecretpassword`. 
Кроме этого будет создана таблица описанная в файле `init.sql` и индексный столбец `TypeOperation`.  

## Подключение psql
Для удобства отладки скриптов и просмотра состояния БД мы будем использовать psql. Чтобы подключиться к уже запущенному ранее контейнеру необходимо выполнить команду ниже:
```shell script
docker exec -it postgresqll psql --username docker --dbname loadtest
``` 
После выполнения команды необходимо ввести пароль указанный в docker-compose.yml - `mysecretpassword`. 

## Блокировки в БД
### Блокировки строк
Для просмотра блокировок строк мы будем использовать расширение [pgrowlocks](https://postgrespro.ru/docs/postgrespro/12/pgrowlocks), чтобы его установить выполните команду в psql:
```postgresql
CREATE EXTENSION pgrowlocks;
```
Далее для просмотра блокировок необходимо будет выполнять запрос:
```postgresql
SELECT * FROM pgrowlocks('Accounts');
```
В результате мы получим [таблицу вида](https://postgrespro.ru/docs/postgrespro/12/pgrowlocks#PGROWLOCKS-COLUMNS):

| Имя | Тип |	Описание |
| --- |---| ---|
|locked_row | tid | Идентификатор кортежа (TID) блокированной строки |
|locker | xid | Идентификатор блокирующей транзакции или идентификатор мультитранзакции, если это мультитранзакция |
|multi | boolean | True, если блокирующий субъект — мультитранзакция |
|xids | xid[] | Идентификаторы блокирующих транзакций (больше одной для мультитранзакции) |
|modes | text[] | Режим блокирования (больше одного для мультитранзакции), массив со значениями Key Share, Share, For No Key Update, No Key Update, For Update, Update. |
|pids | integer[] | Идентификаторы блокирующих обслуживающих процессов (больше одного для мультитранзакции) |

### Список ожиданий процессов
Для просмотра всего списка ожиданий мы будем использовать [pg_stat_activity](https://postgrespro.ru/docs/postgresql/10/monitoring-stats#PG-STAT-ACTIVITY-VIEW).
```postgresql
SELECT pid, state_change, wait_event_type, wait_event, pg_blocking_pids(pid) 
FROM pg_stat_activity 
WHERE backend_type = 'client backend';
```

## Просмотр плана запросов  
Для просмотра планов запросов и поиска не оптимальных запросов мы будем использовать:
```postgresql
EXPLAIN UPDATE Accounts SET amount = 1 WHERE amount < 1000 AND typeoperation = 'Debit';
```
Перед началом каждого запроса для которого мы хотим посчитать его "цену" и увидеть его план.

Если вы, что-то добавили в таблицу, то необходимо сделать принудительно обновление статистики, чтобы не ждать пока это произойдет автоматически. Для этого необходимо выполнить запрос:
```postgresql
EXPLAIN (ANALYZE);
```

## Дополнительная информация
### Блокировки
Если вы хотите более подробно ознакомиться с механизмами блокировок и изучить способы их устранения, то следует ознакомиться со следующей информацией:
* https://habr.com/ru/company/postgrespro/blog/462877/
* https://habr.com/ru/company/postgrespro/blog/442804/
* https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/backend/storage/lmgr/README;hb=HEAD
### Мониторинг
Обычно строится на основе анализа статистики запросов из таблиц/представлений - `pg_stat_*`, которая отлично реализована в PostgreSQL. Более подробно:
* https://habr.com/ru/post/486710/
* https://habr.com/ru/company/okmeter/blog/420429/
* http://pgcluu.darold.net/

## Запуск сценариев нагрузки
Запуск тестов происходит по 3 заранее готовым сценариям:
* `gatling:testOnly *MaxPerformnaceTest` - запуск теста максимальной производительности;
* `gatling:testOnly *Stability` - запуск теста стабильности;
* `gatling:testOnly *DebugTest` - запуск отладочного теста, все запросы его проксируются на порт 8888. 

Параметры запусков тестов определены в файле `simulation.conf`. 
