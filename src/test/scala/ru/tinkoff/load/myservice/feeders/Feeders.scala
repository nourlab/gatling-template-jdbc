package ru.tinkoff.load.myservice.feeders

import ru.tinkoff.gatling.feeders._
import io.gatling.core.Predef._

object Feeders {

  val amount = RandomDigitFeeder("Amount")

  val account = csv("pools/clients.csv").random
}
