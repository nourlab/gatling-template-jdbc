package ru.tinkoff.load

import ru.tinkoff.gatling.config.SimulationConfig._
import ru.tinkoff.load.jdbc.Predef._
import ru.tinkoff.load.jdbc.protocol.JdbcProtocolBuilder
import scala.util.Random

package object myservice {

  val jdbcProtocol: JdbcProtocolBuilder = DB
    .url(getStringParam("db_url"))
    .username(getStringParam("db_username"))
    .password(getStringParam("db_password"))

}
